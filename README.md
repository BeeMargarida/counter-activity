# Counter-Activity

Mobile application that allows the creation of multiple counter at the same time (currently it only allows for 5). 
Developed in React Native, it consists of a simple project to consolidate my experience with the technology.

Here are some screenshots of the application: 

<div align="center">
    ![Counters](images/counters.png)
    ![Add Counter](images/add_counter.png)
</div>
